package com.intro.pages;

import com.crowdar.bdd.cukes.SharedDriver;
import com.crowdar.core.PropertyManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class HomePage extends PhpTravelBasePage {

    private WebElement blogMenuA(){return getWebElement(By.xpath("//a[contains(text(),'Blog')]"));}

    public HomePage(SharedDriver driver) {
        super(driver);
    }

    public void goHere(){
        getDriver().get(PropertyManager.getProperty("phptravel.url"));
    }

    public void goToBlog(){
        blogMenuA().click();
    }
}

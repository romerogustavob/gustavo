package com.intro.pages;

import com.crowdar.bdd.cukes.SharedDriver;
import com.crowdar.core.CucumberPageBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.util.concurrent.TimeUnit;

public class PhpTravelBasePage extends CucumberPageBase {



    public PhpTravelBasePage(SharedDriver driver) {
        super(driver);
    }

    public void waitToLoad() {
        waitToLoad(2);
    }

    public void waitToLoad(int seconds) {
        sleep(seconds * 1000);
    }


    /**
     * Use this if you need more than the default 30 seconds wait
     * @param element
     */
    public void fluidWaitForElement(WebElement element){
        Wait<WebDriver> wait = new FluentWait<>(driver).withTimeout(60L, TimeUnit.SECONDS).pollingEvery(5L, TimeUnit.MILLISECONDS);
        wait.until(ExpectedConditions.visibilityOf(element));
    }


    public boolean isElementVisible(String cssLocator){
        return driver.findElement(By.cssSelector(cssLocator)).isDisplayed();
    }
}

package com.intro.steps;


import com.crowdar.bdd.cukes.SharedDriver;
import com.crowdar.core.PageSteps;
import com.intro.pages.BlogPage;
import com.intro.pages.HomePage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class HomeSteps extends PageSteps {


    private HomePage homePage;
    private BlogPage blogPage;
    public HomeSteps(SharedDriver driver) {
        super(driver);
        homePage = new HomePage(driver);
        blogPage = new BlogPage(driver);

    }

    // Given steps
    @Given("^The user is in home page$")
    public void theUserIsInHomePage(){
        homePage.goHere();
    }


    //end Given steps

    //When steps

    @When("^The user go to (.*) page$")
    public void theUserGoToBlogPage(String menu){
        switch (menu){
            case "Blog" : homePage.goToBlog();
        }
    }


    @Then("The (.*) page title is displayed")
    public void theHomePageTitleIsDiplayed(String page){
        switch (page){
            case "Blog" : blogPage.verifyBlogPageTitle();

        }

    }

    //end Then steps

}
